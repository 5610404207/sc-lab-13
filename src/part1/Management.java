package part1;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Management {
	
	private Lock manageCountLock;
	private Condition manageCondition;
	private LinkedList<Integer> myLink;
	private int newElement;
	
	public Management() {
		manageCountLock = new ReentrantLock();
		manageCondition = manageCountLock.newCondition();
		myLink = new LinkedList<Integer>();
	}
	
	public void add() {
		manageCountLock.lock();
		try {
			myLink.add(1);
			System.out.println("Element added into LinkedList");
			manageCondition.signalAll();
		}
		finally {
			manageCountLock.unlock();
		}
	}
	
	public void remove() throws InterruptedException {
		manageCountLock.lock();
		try {
			while(myLink.size()==0){
				manageCondition.await();

			}
			myLink.removeLast();
			System.out.println("Element removed into LinkedList");
				
		}
		finally {
			manageCountLock.unlock();
		}
	}
	public int getElement(int element){
		return element;
	}

	
}
