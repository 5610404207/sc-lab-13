package part2;

import java.util.ArrayList;

public class Queue {
	private ArrayList<String> list;
	private int size;

	public Queue(int size) {
		list = new ArrayList<String>();
		this.size = size;
	}

	public void add(String n) {
		list.add(n);
	}

	public void remove(int n) {
		list.remove(n);

	}

	public int checkSize() {
		return list.size();
	}

	public String toString() {
		return list.toString();
	}
}
