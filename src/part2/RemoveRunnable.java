package part2;

public class RemoveRunnable implements Runnable{
	private ManageQueue mq;
	private int count;
	private String n;
	private static int DELAY = 1;
	
	public RemoveRunnable(ManageQueue mq,int count,String n){
		this.mq = mq;
		this.count = count;
		this.n = n;
		
	}

	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				mq.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}
	
	
}
