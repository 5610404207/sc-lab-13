package part2;

public class QueueMain {

	public static void main(String[] args) {
		ManageQueue c = new ManageQueue();
		AddRunnable ar = new AddRunnable(c, 100, "w");
		RemoveRunnable rr = new RemoveRunnable(c, 100, "w");

		new Thread(ar).start();

		new Thread(rr).start();

	}

}
