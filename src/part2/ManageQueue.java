package part2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ManageQueue {
	private Lock BalanceChangedLock;
	private Condition enoughFundCondition;
	private Queue queue;

	public ManageQueue() {

		BalanceChangedLock = new ReentrantLock();
		enoughFundCondition = BalanceChangedLock.newCondition();
		queue = new Queue(10);
	}

	public void add(String amount) throws InterruptedException {
		try {
			BalanceChangedLock.lock();

			while (queue.checkSize() > 10) {
				enoughFundCondition.await();
			}
			
			queue.add(amount);

			System.out.print("Add : " + amount + "\n");
			System.out.println(queue.toString());
			enoughFundCondition.signalAll();
		} finally {
			BalanceChangedLock.unlock();
		}
	}

	public void remove(String amount) throws InterruptedException {
		try {
			BalanceChangedLock.lock();
			while (queue.checkSize() <= 0) {
				enoughFundCondition.await();
			}
			
			queue.remove(0);
			
			System.out.print("Remove : " + amount + "\n");
			System.out.println(queue.toString());
			enoughFundCondition.signalAll();
		} finally {
			BalanceChangedLock.unlock();
		}

	}

}
